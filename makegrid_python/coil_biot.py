import numpy as np
import plotly.graph_objects as go
from copy import deepcopy




def coil_biot(coildata, point):

    """
    Usage:

    coildata = read_coils('coils.txt')
    point = np.array([x,y,z]) -- [2.660556e-01,1.297578e-02,-6.691423e-04]
    B = coil_biot(coildata, point)
    """

    Btot = []
    mu_0 = 1e-7

    for key in coildata.keys():
        if key != 'periods':

            x, y, z, cur = coildata[key]
            data = np.array([x, y, z])
            Rf = point - data.T            
            a,b = Rf.shape   

            coef = mu_0*cur[0:len(cur)-1]

            L = np.sqrt(np.sum((np.diff(Rf,axis=0))**2, axis=1))

            unit_vectors = (np.diff(Rf, axis=0).T/L).T

            Rfmag = np.sqrt(np.sum(Rf**2,axis=1))

            num = 2*L*(Rfmag[0:a-1]+Rfmag[1:a])
            denom = (Rfmag[0:a-1]*Rfmag[1:a])*((Rfmag[0:a-1]+Rfmag[1:a])**2-L**2)

            coef2 =num/denom

            unit_vectors = np.multiply(unit_vectors.T, coef)

            Rf = Rf[0:a-1].T*coef2
            
            B = np.cross(unit_vectors.T, Rf.T)

            #cross product
            """
            Bx = np.multiply(unit_vectors[1,:],Rf[2,:]) - np.multiply(unit_vectors[2,:],Rf[1,:])

            By = -(np.multiply(unit_vectors[0,:],Rf[2,:]) - np.multiply(unit_vectors[2,:],Rf[0,:]))

            Bz = np.multiply(unit_vectors[0,:],Rf[1,:]) - np.multiply(unit_vectors[1,:],Rf[0,:])

            B = np.array([Bx, By, Bz]) 

            Still yields negative value for Bz component respect to BS-SOLCTRA data.
            """

            Btot.append(np.sum(B, axis=0)) #axis=1 for hardcoded cross product

    Btot = np.array(Btot)

    return np.sum(Btot, axis=0)

def RK4(f, a, b, N, p0):
    
    # Función que implementa el algoritmo de Runge-Kutta 4 
    
    h = (b-a)/N
    p = [p0]
    ite = np.linspace(a,b,N+1)
    for i in range(N+1):
        
        k1 = h*f(ite[i],p[i])
        k2 = h*f(ite[i]+h/2, p[i]+k1/2)
        k3 = h*f(ite[i]+h/2, p[i]+k2/2)
        k4 = h*f(ite[i]+h, p[i]+k3)
        
        res = p[i] + (1/6)*(k1+2*k2+2*k3+k4)
                 
        p.append(res)
    return p, ite
s





    





